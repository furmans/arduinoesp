#include <SoftwareSerial.h>

/*
  FurmanS 2015
  KLASA REPREZENTUJACA MODUL WIFI!
  POSIADA STAN ORAZ KILKA PROSTYCH METOD
  AT+CWJAP="nazwaSieci","hasloSieci"
  AT+CIPSTART="TCP","184.106.153.149",80
  AT+CIPSEND=34
*/

class WiFi{
    
  private:
    
    String WifiNetworkNameSSID="SSID";
    String WifiPassword="haslo";  
    static const int SERIAL_RX = 2;
    static const int SERIAL_TX = 3;

    bool wifiPresent; // reprezentuje stan czujnika 
    
    SoftwareSerial esp8266 = SoftwareSerial(this->SERIAL_RX, this->SERIAL_TX);
    
    void initial(void)
      {
        this->esp8266.begin(9600);
        this->esp8266.flush();
        this->esp8266.setTimeout(2000);
        //this->resetModule();
        //this->readESPResponse();  
        delay(1000);
         // dlaczego dziala tylko tutaj ?
        this->joinToWiFiAccessPoint();
        this->readESPResponse(); 
    
      }
        
  public:
    // pubiczy konstruktor obiektu
    WiFi(){
      initial();
    }
    
    void joinToWiFiAccessPoint(){
      String wifiConnectCommand="AT+CWJAP=\"" + WifiNetworkNameSSID + "\",\"" + WifiPassword + "\"";
      this-esp8266.println(wifiConnectCommand);
    }
    
    void resetModule(void){
      this->esp8266.println("AT+RST");
    }
    
    void getAvailableWiFi(void){
      this->esp8266.println("AT+CWLAP");
    }    
    
    void tcpConnectAndSendGET(){
       String cmd = "AT+CIPSTART=\"TCP\",\"";
              cmd += "184.106.153.149"; // api.thingspeak.com
              cmd += "\",80";
              Serial.println(cmd);
              this->esp8266.println(cmd);
              
               // prepare GET string
              String getStr = "GET /update?api_key=";
              getStr +="&field1=";
              getStr += String(10);
              getStr += "\r\n\r\n";
            Serial.println(getStr);
              // send data length
              String cmdForGETRequest = "AT+CIPSEND=";
              cmdForGETRequest += String(getStr.length());
              Serial.println(cmdForGETRequest);
              this->esp8266.println(cmdForGETRequest);
              
    }
    
    
    bool checkWiFiPresent(){
       return this->wifiPresent;
    } 
  
     void readESPResponse(){

      if(this->esp8266.available()) // check if the esp is sending a message
        {
        while(this->esp8266.available())
            {
            //this->wifiPresent = true;
            // The esp has data so display its output to the serial window
            char c = this->esp8266.read(); // read the next character.
            Serial.print(c);
            }
          
        }
      }
      
    };
    
// ref. do modulu WiFi
WiFi *wifi;

void setup(){
  Serial.begin(9600);
  wifi = new WiFi();
  Serial.println("||");
  Serial.println(wifi->checkWiFiPresent());
  Serial.println("||");
  wifi->tcpConnectAndSendGET();
 
}

void loop(){
  
}